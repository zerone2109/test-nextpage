<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/', 'EmployeeController@home');
Route::get('/list', 'EmployeeController@list')->middleware('auth');
Route::resource('/employees', 'EmployeeController');
Route::get('/tree', 'EmployeeController@tree');
Route::get('/treenode', 'EmployeeController@treenode');
Route::get('/searchchiefs', 'EmployeeController@searchChiefs');
Route::get('/positions', 'PositionController@index');