<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('employees', function(Blueprint $table) {
      $table->increments('id');
      $table->integer('parent_id')->nullable()->index();
      $table->integer('lft')->nullable()->index();
      $table->integer('rgt')->nullable()->index();
      $table->integer('depth')->nullable();
      $table->string('firstname', 30);
      $table->string('middlename', 30);
      $table->string('lastname', 30);
      $table->integer('position_id')->unsigned();
      $table->decimal('salary', 8, 2)->nullable();
      $table->date('employed_at');

      $table->timestamps();

      $table->foreign('position_id')->references('id')->on('positions');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    $table->dropForeign('employees_position_id_foreign');
    Schema::drop('employees');
  }

}
