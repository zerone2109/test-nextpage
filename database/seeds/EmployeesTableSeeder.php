<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employee_count = 50000;
        $director_count = 1;
        $depth_tree = 5;
        $count = 0;
        $for_count = ($employee_count - $director_count) / $depth_tree;
        $parent = 0;
        $offset = 0;
        $faker = Faker\Factory::create('ru_RU');
        $gender = 'male';
        for ($i = 0; $i < $director_count; $i++){
            $gender = $faker->randomElements(['male', 'female'])[0];
            $ru_lastname = $faker->lastName();
            if ($gender == 'female'){
                $ru_lastname .= 'а';
            }
            factory(App\Employee::class)->create([
                'firstname' => $faker->firstName($gender),
                'middlename' => $faker->middleName($gender),
                'lastname' =>  $ru_lastname,
                'salary' => $faker->numberBetween($min = 1200, $max = 1500),
                'employed_at' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'position_id' => function () {
                    return App\Position::get()->first()->id;
                },
                'parent_id' => null,
            ]);
        }

        factory(App\Employee::class, $employee_count - $director_count)->create();
        for( $i = 0; $i < $depth_tree; $i++ ){
            $count = $for_count;
            if ( $i == 0 ){
                $count = $director_count;
            }
            if ( $i == 1){
                $count = $employee_count / $depth_tree ** 2;
            }
            if ( $i == $depth_tree - 1 ){
                $count =  $employee_count;
            }
            if (  $employee_count != 0 ){   
                $employees = App\Employee::all()->slice($offset, $count, true);
                foreach ($employees as $employee){
                    if ( $i != 0 ) { 
                        $employee->makeChildOf($parent->random());
                    }
                    $employee_count--;
                    $offset++;
                }
                $parent = $employees;
            }
        }


    }
}
