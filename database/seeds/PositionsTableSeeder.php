<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PositionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('positions')->delete();

        $array_pos = array("Developer",
                             "Secretary",
                             "Accountant",
                             "Designer",
                             "Typesetter",
                             "Manager",
                             "Driver",
                             "Mechanic",
                             "Locksmith",
                             "Commodity expert"
                            );
        $position = factory(App\Position::class)->create([
            'name' => 'Director',
          ]);
        foreach($array_pos as $pos){
            $position = factory(App\Position::class)->create([
                'name' => $pos,
              ]);
        };

    }
}
