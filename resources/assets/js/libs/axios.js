import axios from 'axios';

    axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    axios.defaults.headers.common['Content-type'] = 'application/json';

export default axios;