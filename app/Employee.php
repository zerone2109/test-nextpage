<?php
namespace App;

use Fico7489\Laravel\EloquentJoin\Traits\EloquentJoinTrait;
use Baum\Node;

class Employee extends Node {

  use EloquentJoinTrait;

  public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        
        $this->useTableAlias = true;
    }

  /**
   * Table name.
   *
   * @var string
   */
  protected $table = 'employees';

  protected $fillable = ['firstname', 'middlename', 'lastname', 'salary', 'position_id','employed_at', 'parent_id'];

  public function position() {
    return $this->belongsTo(Position::class);
  }

  public function parent(){
    return $this->belongsTo(self::class, 'parent_id');
  }

  public function toSearchableArray()
  {
    $array = [
      'firstname' => $this->firstname,
      'middlename' => $this->middlename,
      'lastname' => $this->lastname,
      'salary' => $this->salary,
    ];

    return $array;
  }


}
