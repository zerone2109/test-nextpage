<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use App\Http\Requests\StoreEmployeeRequest;
use App\Http\Resources\EmployeeResource;
use App\Http\Resources\EmployeeTreeResource;
use Carbon\Carbon;
use DateTime;

use App\Http\Controllers\Controller;

class EmployeeController extends Controller
{
    public function home(){
        return view('employees.tree');
    }

    public function list(){
        return view('employees.list');
    }

    public function tree(Request $request){
        $id = null;
        if ($request->filled('id')) {
            $id = $request->id;
            $employee = Employee::find($id);
            if ($employee){
                $employees = $employee->immediateDescendants()->get();
                return EmployeeResource::collection($employees);
            }
        } else 
        {
            $employees = Employee::roots()->get();
            return EmployeeResource::collection($employees);
        } 
    }

    public function index(Request $request)
    {
        $temp = '%';
        $col = null;
        $sort = null;
        $search =  null;
        $salaryFrom = 0;
        $salaryTo = 0;
        $dateFrom = null;
        $dateTo = null;
        $cpar = false;
        
        if ($request->filled('c')) {
            $col = $request->c;
        }
        if ($request->filled('s')) {
            $sort = $request->s;
        }
        if ($request->filled('search')) {
            $search .= $temp;
            $search .= $request->search;
            $search .= $temp;
        }
        if ($request->filled('cpar')) {
            $cpar = $request->cpar;
        }
        if ($request->filled('searchst')) {
            $salaryTo = $request->searchst;
        }
        if ($request->filled('searchft')){
            $salaryFrom = $request->searchft;
        }
        if ($request->filled('df')){
            $dateFrom = $request->df;
        }
        if ($request->filled('dt')){
            $dateTo = $request->dt;
        }
        $employee = null;

        if ($cpar) {
            $employee = Employee::whereJoin('lastname', 'LIKE', $search)
                                ->orWhereJoin('firstname', 'LIKE', $search)
                                ->orWhereJoin('middlename', 'LIKE', $search)
                                ->orWhereJoin('salary', 'LIKE', $search)
                                ->orWhereJoin('position.name', 'LIKE', $search)
                                ->orWhereJoin('parent.lastname', 'LIKE', $search);
        }
        else {
            $employee = Employee::whereJoin('lastname', 'LIKE', $search)
                                ->orWhereJoin('firstname', 'LIKE', $search)
                                ->orWhereJoin('middlename', 'LIKE', $search)
                                ->orWhereJoin('salary', 'LIKE', $search)
                                ->orWhereJoin('position.name', 'LIKE', $search);
        }
        if ($salaryFrom > 0){
            $employee = $employee->whereJoin('salary', '>=', $salaryFrom);
        }
        if ($salaryTo > 0){
            $employee = $employee->whereJoin('salary', '<=', $salaryTo);
        }
        if ($dateFrom){
            $employee = $employee->whereJoin('employed_at', '>=', $dateFrom);           
        }
        if ($dateTo){
            $employee = $employee->whereJoin('employed_at', '<=', $dateTo);
        }

        if ($employee){
            $employee = $employee->orderByJoin($col, $sort)->paginate(15);           
        }
        else {
            $employee = Employee::orderByJoin($col, $sort)->paginate(15);  
        }

        return EmployeeResource::collection($employee);
    }

    public function create(Request $request)
    { 
        $employee = Employee::create($request->all());
        $employee->save();
        return $employee;
    }

    public function store(StoreEmployeeRequest $request)
    {
        $employee = Employee::create($request->all());
        $employee->save();

        return $employee;
    }

    public function show($id)
    {
        $employee = Employee::find($id);
        return new EmployeeResource($employee);

    }

    public function edit($id)
    {
        $employee = Employee::find($id);
        return new EmployeeResource($employee);
    }

    public function update(StoreEmployeeRequest $request, $id)
    {
        $employee = Employee::find($id);

		if($employee->count()){
			 $employee->update($request->all());
			 return response()->json(['status'=>'success','msg'=>'Employee updated successfully.']);
		}else{
			 return response()->json(['status'=>'error','msg'=>'Error in updating Employee']);
		}
    }

    public function destroy($id)
    {
        $employee = Employee::find($id);
		if($employee->count()){
			$employee->delete();
			return response()->json(['status'=>'success','msg'=>'Employee deleted successfully']);
		}else{
			 return response()->json(['status'=>'error','msg'=>'Error in deleting Employee']);
		}
    }

    public function searchChiefs (Request $request){
        $temp = '%';
        $search =  '';
        $employee = null;
        if ($request->filled('query')) {
            $search = $temp;
            $search .= $request->get('query');
            $search .= $temp;
        }
        $employee = Employee::whereJoin('lastname', 'LIKE', $search)
                            ->orWhereJoin('firstname', 'LIKE', $search)
                            ->orWhereJoin('middlename', 'LIKE', $search)
                            ->get();
        return EmployeeResource::collection($employee);
    }
}
