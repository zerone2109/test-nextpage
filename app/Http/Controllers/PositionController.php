<?php

namespace App\Http\Controllers;
use App\Position;
use App\Http\Resources\PositionResource;

use Illuminate\Http\Request;

class PositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $temp = '%';
        $search =  null;
        $positions = null;
        if ($request->filled('query')) {
            $search = $temp;
            $search .= $request->get('query');
            $search .= $temp;
        }
        if ($search)
            $positions = Position::whereJoin('name', 'LIKE', $search)->get();
        else 
            $positions = Position::all();

        return PositionResource::collection($positions);
    }
}
